/*
 *  Filename: 1wire.c
 *
 *  Created on: 12-01-2013
 *      Author: pl4kat@gmail.com
 */
#include "1wire.h"
#include <util/delay.h>

void ow_tx_bit(ui8 bit)
{
	SET_PIN_AS_OUTPUT
	OW_PIN_LOW

	if(bit)
	{
		//wait 15 microseconds
		_delay_us(11);

		//and release the bus
		SET_PIN_AS_INPUT
		_delay_us(62);
	}
	else
	{
		_delay_us(62);

		//and release the bus
		SET_PIN_AS_INPUT
		_delay_us(11);
	}

	return;
}

ui8 ow_rx_bit()
{
	ui8 bit = 0;

	//put bus to low state to initiate read
	SET_PIN_AS_OUTPUT
	OW_PIN_LOW

	//..wait just a while ;)
	_delay_us(11);

	//release the bus
	SET_PIN_AS_INPUT

	//wait another while
	_delay_us(11);

	//read pin state
	bit = OW_PIN_VALUE

	//block to the end of the time slot
	_delay_us(62);

	return bit;
}

void ow_tx_byte(ui8 byte)
{
	for(ui8 i=0; i<8; i++)
		ow_tx_bit(byte & (1<<i));

	return;
}

ui8 ow_rx_byte()
{
	ui8 data = 0, bit = 0;

	for(ui8 i=0; i<8; i++)
	{
		bit = ow_rx_bit();
		data |= bit<<i;
	}

	return data;
}

ui8 ow_reset_pulse()
{
	ui8 presence = 0;

	//put the bus in low state
	SET_PIN_AS_OUTPUT
	OW_PIN_LOW

	//and wait 480us minimum
	_delay_us(240);
	_delay_us(240);

	//release the bus
	SET_PIN_AS_INPUT

	//wait some time to read presence pulse
	_delay_us(60);

	presence = OW_PIN_VALUE

	//and wait 480us minimum
	_delay_us(240);
	_delay_us(240);

	return presence;
}
