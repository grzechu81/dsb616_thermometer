/*
 *  Filename: common.h
 *
 *  Created on: 11-01-2013
 *      Author: pl4kat@gmail.com
 */

#ifndef COMMON_H_
#define COMMON_H_
#include <avr/io.h>

//types
typedef unsigned char ui8;
typedef unsigned int  ui16;

#define TRUE 1
#define FALSE 0

//macros
#define SET(reg, pin) reg |= (1<<pin)
#define CLR(reg, pin) reg &= ~(1<<pin)
#define TOGGLE(reg, pin) reg ^= (1<<pin)

////digits
#define DIGIT1 PB3
#define DIGIT2 PB2
#define DIGIT3 PB1
#define DIGIT4 PB0

#endif /* COMMON_H_ */
