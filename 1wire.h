/*
 *  Filename: 1wire.h
 *
 *  Created on: 12-01-2013
 *      Author: pl4kat@gmail.com
 */
#ifndef ONEWIRE_H_
#define ONEWIRE_H_

#include "common.h"

//1-Wire commands
#define OW_SKIP_ROM 		0xcc
#define OW_CONV_T 			0x44
#define OW_READ_SCRATCH 	0xbe
#define OW_WRITE_SCRATCH 	0x4e

//Pin
#define OW_PIN			PA0
#define OW_PIN_DIR_REG 	DDRA
#define OW_PIN_PORT 	PORTA
#define OW_PIN_PIN		PINA

//Macros
#define SET_PIN_AS_OUTPUT OW_PIN_DIR_REG |= (1<<OW_PIN);
#define SET_PIN_AS_INPUT  OW_PIN_DIR_REG &= ~(1<<OW_PIN);
#define OW_PIN_HIGH	OW_PIN_PORT |= (1<<OW_PIN);
#define OW_PIN_LOW	OW_PIN_PORT &= ~(1<<OW_PIN);
#define OW_PIN_VALUE (OW_PIN_PIN & (1<<OW_PIN)) >> OW_PIN;

//RX TX functions
void ow_tx_bit(ui8 bit);
ui8  ow_rx_bit();
void ow_tx_byte(ui8 byte);
ui8  ow_rx_byte();

//Instructions
ui8 ow_reset_pulse();

#endif /* ONEWIRE_H_ */
