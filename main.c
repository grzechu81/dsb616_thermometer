/*
 *  Filename: main.c
 *
 *  Created on: 11-01-2013
 *      Author: pl4kat@gmail.com
 */
#include "common.h"
#include "1wire.h"
#include "avr/interrupt.h"
#include <avr/pgmspace.h>

//digits: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, (BLANK), -, E
static ui8 g_digit[13] PROGMEM = {0x40, 0x79, 0x24, 0x30, 0x19, 0x12, 0x2, 0x78, 0x0, 0x10, 0x7f, 0x3f, 0x6};
ui8 g_displays[4];
ui8 g_disp_buf[4];
volatile ui8 g_position;
volatile ui8 g_ds18b20_trig = FALSE;
ui8 g_ds18b20_temp_conv = FALSE;

void init_digits();
void start_timers();
void sensor_config();
void sensor_check_temp();
void sensor_get_temp();
ui8  sensor_crc(ui8 crc, ui8 data);
void update_lcd_buffer(ui8* data);

int main(void)
{
	init_digits();
	start_timers();
	sensor_config();

	//set all digits to blank
	g_disp_buf[0] = pgm_read_byte(&g_digit[10]);
	g_disp_buf[1] = pgm_read_byte(&g_digit[10]);
	g_disp_buf[2] = pgm_read_byte(&g_digit[10]);
	g_disp_buf[3] = pgm_read_byte(&g_digit[10]);

	sei();

	while(TRUE)
	{
		if(g_ds18b20_trig == TRUE)
		{
			//master issues reset pulse
			ui8 presence = ow_reset_pulse();

			//if no device found, show on display E1
			if(presence != 0)
			{
				g_disp_buf[0] = pgm_read_byte(&g_digit[10]);
				g_disp_buf[1] = pgm_read_byte(&g_digit[12]); //E
				g_disp_buf[2] = pgm_read_byte(&g_digit[2]);  //2
				g_disp_buf[3] = pgm_read_byte(&g_digit[10]);
			}
			else
			{
				//was temp converted last time ?
				if(g_ds18b20_temp_conv == TRUE)
				{
					sensor_get_temp();
					g_ds18b20_temp_conv = FALSE;
				}
				else
				{
					sensor_check_temp();
					g_ds18b20_temp_conv = TRUE;
				}
			}

			g_ds18b20_trig = FALSE;
		}

	}
	return 0;
}

ISR(TIMER0_COMPA_vect)
{
	PORTD = g_disp_buf[g_position];
	PORTB = (1<<g_displays[g_position]);

	g_position++;
	if(g_position > 3)
	{
		g_position = 0;
	}

	return;
}

ISR(TIMER1_COMPA_vect)
{
	if(g_ds18b20_trig == FALSE)
	{
		g_ds18b20_trig = TRUE;
	}

	return;
}

void init_digits()
{
	//displays
	g_displays[0] = DIGIT1;
	g_displays[1] = DIGIT2;
	g_displays[2] = DIGIT3;
	g_displays[3] = DIGIT4;

	//set up the registers
	DDRB = 0x0f; //all digits pin as outputs
	DDRD = 0x7f; //all segments pins as outputs
	return;
}

void start_timers()
{
	//display timer
	TCCR0A = (1<<WGM01); //CTC mode
	TCCR0B = (1<<CS02); //256 prescaler
	OCR0A = 0x9b; //5msec
	g_position = 0;
	TIMSK = (1<<OCIE0A);

	//temp checking timer
	TCCR1B = (1<<WGM12) | (1<<CS12); //CTC mode, 256 presc
	OCR1A = 0x7A11; //1s;
	TIMSK |= (1<<OCIE1A);

	return;
}

void sensor_config()
{
	//master issues reset pulse
	ui8 presence = ow_reset_pulse();

	//if no device found, show on display E1
	if(presence != 0)
	{
		g_disp_buf[0] = pgm_read_byte(&g_digit[10]);
		g_disp_buf[1] = pgm_read_byte(&g_digit[12]); //E
		g_disp_buf[2] = pgm_read_byte(&g_digit[1]);  //1
		g_disp_buf[3] = pgm_read_byte(&g_digit[10]);

		return;
	}

	//skip addressing
	ow_tx_byte(OW_SKIP_ROM);

	//issue write scratchpad command
	ow_tx_byte(OW_WRITE_SCRATCH);

	//write into scratchpad
	ow_tx_byte(0);    //no alarm
	ow_tx_byte(0);    //no alarm
	ow_tx_byte(0x40); //11 bit resolution

	return;
}

void sensor_check_temp()
{
	//skip addressing
	ow_tx_byte(OW_SKIP_ROM);

	//convert T
	ow_tx_byte(OW_CONV_T);

	return;
}

void sensor_get_temp()
{
	ui8 crc = 0;

	//skip addressing
	ow_tx_byte(OW_SKIP_ROM);

	//read scratchpad
	ow_tx_byte(OW_READ_SCRATCH);

	ui8 in_buffer[9] = {0};
	for(ui8 i=0; i < 9; i++)
	{
		in_buffer[i] = ow_rx_byte();

		//calculate CRC value for all bytes except CRC byte
		if(i != 8)
		{
			crc = sensor_crc(crc, in_buffer[i]);
		}
	}

	if(crc == in_buffer[8])
	{
		update_lcd_buffer(in_buffer);
	}

	return;
}

ui8 sensor_crc(ui8 crc, ui8 data)
{
	ui8 fb = 0;
	for(ui8 i=0; i<8; ++i)
	{
		//xor every data bit with lsb of crc
		fb = (crc & 0x1) ^ ((data >> i) & 0x01);

		//shift crc one left
		crc = crc >> 1;

		if(fb == 1)
		{
			crc = crc ^ 0x8c; //crc ^ b10001100
		}
	}
	return crc;
}

void update_lcd_buffer(ui8* data)
{
	ui8 int_part = 0;
	ui8 flt_part = 0;
	ui16 temp = (data[1] << 8) | data[0];
	ui8 sign = (temp & 0x800) >> 11;

	//if 1, negative number
	if(sign)
	{
		//first invert bits, then add 1
		temp = ~temp;
		temp += 1;
	}

	//integer part
	int_part = (temp & 0xff0) >> 4;
	//floating point part
	flt_part = (temp & 0xf) >> 1 ;

	g_disp_buf[0] = (sign == 0 ? pgm_read_byte(&g_digit[10]) : pgm_read_byte(&g_digit[11]));
	g_disp_buf[1] = pgm_read_byte(&g_digit[int_part/10]);
	g_disp_buf[2] = pgm_read_byte(&g_digit[int_part%10]);

	switch(flt_part)
	{
		case 0:
			g_disp_buf[3] = pgm_read_byte(&g_digit[0]);
			break;
		case 1:
			g_disp_buf[3] = pgm_read_byte(&g_digit[1]); //.125
			break;
		case 2:
			g_disp_buf[3] = pgm_read_byte(&g_digit[2]); //.250
			break;
		case 3:
			g_disp_buf[3] = pgm_read_byte(&g_digit[4]); //.375
			break;
		case 4:
			g_disp_buf[3] = pgm_read_byte(&g_digit[5]); //.500
			break;
		case 5:
			g_disp_buf[3] = pgm_read_byte(&g_digit[6]); //.625
			break;
		case 6:
			g_disp_buf[3] = pgm_read_byte(&g_digit[7]); //.750
			break;
		case 7:
			g_disp_buf[3] = pgm_read_byte(&g_digit[9]); //.875
			break;
		default:
			break;
	}

	return;
}
